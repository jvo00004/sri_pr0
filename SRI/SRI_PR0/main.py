#importa expresiones regulares
import re
#importa la clase HTMLParser
from html.parser import HTMLParser
#importa la clase normalize
from unicodedata import normalize
#importa la librería de tiempo
import time
#importa la libreria del Sistema Operativo
import os

class HTMLCleaner(HTMLParser):
    title = ""
    body = ""
    ntags = 0
    nfiles = 0
    initime = 0
    fitime = 0
    ntokens = 0

    def handle_data(self, data):
        stag = self.get_starttag_text()
        if stag == "<title>":
            self.title += data + "\n"
            self.ntags += 1
            return self.title
        elif stag == "<p>":
            self.body += data + "\n"
            self.ntags += 1
            return self.body

def leerHTML(fichero, parser):
#se va a abrir un fichero para parsearlo
    with open(fichero, "r", encoding="utf8") as archivo:
        parser.nfiles += 1
        #se inicializa la pagina con cualquier texto para que no esté vacía
        pagina = "hola"
        while True:
            pagina = archivo.readline()
            #si la pagina está vacía, hemos llegado al final del texto
            if pagina == "":
                break
            pagina = str.strip(pagina)
            parser.feed(pagina)
    return parser


def normalizador(fichero):
    titulo = fichero.title
    cuerpo = fichero.body

    titulo = titulo.casefold()
    cuerpo = cuerpo.casefold()

    titulo = re.sub(
        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
        normalize("NFD", titulo), 0, re.I
    )
    cuerpo = re.sub(
        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
        normalize("NFD", cuerpo), 0, re.I
    )

    titulo = titulo.replace(' ', "\n")
    cuerpo = cuerpo.replace(" ", "\n")

    fichero.ntokens += titulo.count("\n")
    fichero.ntokens += cuerpo.count("\n")

    fichero.title = ""
    fichero.body = ""

parser = HTMLCleaner()
parser.initime = time.perf_counter()
ficheros = os.listdir("./coleccionESuja2019/")
for i in range(0, len(ficheros)):
    parser = leerHTML("./coleccionESuja2019/" + ficheros[i], parser)
    normalizador(parser)

parser.fitime = (time.perf_counter() - parser.initime)


print("Tiempo de ejecución: " + str(parser.fitime))
print("Número de ficheros procesados: " + parser.nfiles)
print("Numero de etiquetas: " + parser.ntags)
print("Numero de tokens: " + parser.ntokens)

